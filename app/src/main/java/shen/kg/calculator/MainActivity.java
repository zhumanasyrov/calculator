package shen.kg.calculator;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    Button buttonAdd;
    Button buttonSub;
    Button buttonMult;
    Button buttonDiv;

    EditText etNumOne;
    EditText etNumTwo;

    TextView tvShow;

    String oper = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNumOne = (EditText) findViewById(R.id.etNumOne);
        etNumTwo = (EditText) findViewById(R.id.etNumTwo);

        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonSub = (Button) findViewById(R.id.buttonSub);
        buttonMult = (Button) findViewById(R.id.buttonMult);
        buttonDiv = (Button) findViewById(R.id.buttonDiv);

        tvShow= (TextView) findViewById(R.id.tvShow);

        buttonAdd.setOnClickListener(this);
        buttonSub.setOnClickListener(this);
        buttonMult.setOnClickListener(this);
        buttonDiv.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        float num1 = 0;
        float num2 = 0;
        float result = 0;


        if (TextUtils.isEmpty(etNumOne.getText().toString())
                || TextUtils.isEmpty(etNumTwo.getText().toString())) {
            return;
        }

        num1 = Float.parseFloat(etNumOne.getText().toString());
        num2 = Float.parseFloat(etNumTwo.getText().toString());


        switch (v.getId()) {
            case R.id.buttonAdd:
                oper = "+";
                result = num1 + num2;
                break;
            case R.id.buttonSub:
                oper = "-";
                result = num1 - num2;
                break;
            case R.id.buttonMult:
                oper = "*";
                result = num1 * num2;
                break;
            case R.id.buttonDiv:
                oper = "/";
                result = num1 / num2;
                break;
            default:
                break;
        }

        tvShow.setText(num1 + " " + oper + " " + num2 + " = " + result);
    }
}
